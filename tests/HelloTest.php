<?php

namespace tests;

use test\Hello;
use PHPUnit\Framework\TestCase;

class HelloTest extends TestCase
{
	public function testEchoNumber()
	{
		$obj = new Hello();
		$this->assertTrue(is_numeric($obj->echoNumber()));
	}
}