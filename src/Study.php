<?php

namespace Tool;

class Study
{
	public static function study_login()
	{
		$heade = [
//			'Cookie: PHPSESSID=l6sh8mt2b1m1u62kmdignqfsau',
			'Host: gk.xiguashuwang.com',
			'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0',
			'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
			'Accept-Language: zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2',
			'Accept-Encoding: gzip, deflate',
			'Referer: http://gk.xiguashuwang.com/web/login',
			'Connection: keep-alive',
			'Upgrade-Insecure-Requests: 1',
			'Pragma: no-cache',
			'Cache-Control: no-cache',
		];
		// 1. 初始化
		$ch = curl_init('http://gk.xiguashuwang.com/web/login');
		// 2. 设置选项
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // 设置不直接输出到页面

		curl_setopt($ch, CURLOPT_ENCODING, 'gzip');// 压缩传输 解吗
		curl_setopt($ch, CURLOPT_HTTPHEADER, $heade);

		curl_setopt($ch, CURLOPT_VERBOSE, 0);  // 如果你想CURL报告每一件意外的事情，设置这个选项为一个非零值。

		curl_setopt($ch, CURLOPT_HEADER, 1); //返回response头部信息
		curl_setopt($ch, CURLINFO_HEADER_OUT, true); //TRUE 时追踪句柄的请求字符串，从 PHP 5.1.3 开始可用。这个很关键，就是允许你查看请求header

		// 验证证书
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');

		curl_setopt($ch, CURLOPT_AUTOREFERER, 1); // 当根据Location:重定向时，自动设置header中的Referer:信息。
		// 3. 执行并获取http请求内容
		$str = curl_exec($ch);
		// 4. 释放curl句柄
		curl_close($ch);

		preg_match_all('|Set-Cookie: (.*);|U', $str, $arr);
		$cookie = $arr[1][0];

		$csrf_token = substr($str, 1167, 43);
//		$csrf_token = substr($str, 781, 43);

		$params = '_mobile=%E5%B9%BF%E5%BC%80%E9%A2%98%E5%BA%93%E6%9F%A5%E8%AF%A2&_password=ZP000&_csrf_token=' . $csrf_token;
		$heade = [
			'Cookie: ' . $cookie,
			'Host: gk.xiguashuwang.com',
			'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0',
			'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
			'Accept-Language: zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2',
			'Accept-Encoding: gzip, deflate',
			'Content-Type: application/x-www-form-urlencoded',
			'Origin: http://gk.xiguashuwang.com',
			'Connection: keep-alive',
			'Referer: http://gk.xiguashuwang.com/web/login',
			'Upgrade-Insecure-Requests: 1',
			'Pragma: no-cache',
			'Cache-Control: no-cache',
		];
		// 1. 初始化
		$ch = curl_init('http://gk.xiguashuwang.com/web/login');
		// 2. 设置选项
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // 设置不直接输出到页面

		// post 选择
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

		curl_setopt($ch, CURLOPT_ENCODING, 'gzip');// 压缩传输 解吗
		curl_setopt($ch, CURLOPT_HTTPHEADER, $heade);

		curl_setopt($ch, CURLOPT_VERBOSE, 0);  // 如果你想CURL报告每一件意外的事情，设置这个选项为一个非零值。

		curl_setopt($ch, CURLOPT_HEADER, 1); //返回response头部信息
		curl_setopt($ch, CURLINFO_HEADER_OUT, true); //TRUE 时追踪句柄的请求字符串，从 PHP 5.1.3 开始可用。这个很关键，就是允许你查看请求header

		// 验证证书
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');

		curl_setopt($ch, CURLOPT_AUTOREFERER, 1); // 当根据Location:重定向时，自动设置header中的Referer:信息。
		// 3. 执行并获取http请求内容
		$str = curl_exec($ch);
		preg_match_all('|Set-Cookie: (.*);|U', $str, $arr);
		\Cache::set('study_cookie', $arr[1][0], 3600 * 12);
		return $arr[1][0];
	}

	// 获取学习数据
	public static function get_answer($title)
	{
//		$cookie = \Cache::get('study_cookie');
//		if (!$cookie)
//			$cookie = self::study_login();
		$cookie = 'Hm_lvt_4640b5e2921bdb394d40e9b4a38744b9=1609079222,1610938193; PHPSESSID=hjq65b1dgfdqk2cph0dd8ria1u; Hm_lpvt_4640b5e2921bdb394d40e9b4a38744b9=1610972622';
		if (!$title)
			return Common::re_msg(1, '获取学习数据', [
				'data' => '',
			]);
		$params = 'type=1&title=' . trim($title);
		$heade = [
			'Cookie: ' . $cookie,
			'Host: gk.xiguashuwang.com',
			'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0',
			'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
			'Accept-Language: zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2',
			'Accept-Encoding: gzip, deflate',
			'Content-Type: application/x-www-form-urlencoded',
			'Origin: http://gk.xiguashuwang.com',
			'Connection: keep-alive',
			'Referer: http://gk.xiguashuwang.com/web/login',

			'Upgrade-Insecure-Requests: 1',
			'Pragma: no-cache',
			'Cache-Control: no-cache',
		];
		// 1. 初始化
		$ch = curl_init('http://119.23.141.32/web/index');
//		$ch = curl_init('http://gk.xiguashuwang.com/web/index');
		// 2. 设置选项
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // 设置不直接输出到页面

		// post 选择
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

		curl_setopt($ch, CURLOPT_ENCODING, 'gzip');// 压缩传输 解吗

		curl_setopt($ch, CURLOPT_HTTPHEADER, $heade);

		curl_setopt($ch, CURLOPT_HEADER, 0); // 如果你想把一个头包含在输出中，设置这个选项为一个非零值。
		curl_setopt($ch, CURLOPT_VERBOSE, 0);  // 如果你想CURL报告每一件意外的事情，设置这个选项为一个非零值。

		// 验证证书
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');

		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // 设置这个选项为一个非零值(象 'Location: ')的头，服务器会把它当做HTTP头的一部分发送(注意这是递归的，PHP将发送形如 'Location: '的头)。
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1); // 当根据Location:重定向时，自动设置header中的Referer:信息。
		// 3. 执行并获取http请求内容
		$str = curl_exec($ch);
		if (curl_getinfo($ch)['http_code'] <> 200) {
			\Cache::forget('study_cookie');
			return self::get_answer($title);
		}
		// 4. 释放curl句柄
		curl_close($ch);

		return Common::re_msg(1, '获取学习数据', [
			'data' => $str,
		]);
	}

}